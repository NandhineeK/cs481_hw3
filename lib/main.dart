import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main()
{
  runApp(MaterialApp(
    //home:Homepage(),
  initialRoute: '/',
    routes: {
    '/':(context)=>Homepage(),
  '/second':(context)=>Page2(),
    },
  ));
}

class Homepage extends StatefulWidget {

@override
  _MyHomePageState createState()=>_MyHomePageState();
//var titleSection;
}
  class _MyHomePageState extends State<Homepage>
  {
    int counter=0;

    void incrementCounter(){
    setState((){
    counter++;
  });
  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
   // var titleSection;
    return Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          title: Text('Travel Website - Explore the world '),
        ),
        body: ListView(
            children: [
              Image.asset(
                'assets/paris.jpg',
                fit: BoxFit.cover,
              ),
              Padding(
                padding: EdgeInsets.only(top:20),
                child:Text("France",textAlign: TextAlign.center,
                  style:TextStyle(
                    color:Colors.lightGreenAccent,
                    fontFamily: 'Pacifico',
                    fontSize: 35,
                  ),
                ),
              ),
        Padding(
          padding: EdgeInsets.only(top:20),
          child:Text(" France is a country rich in history,"
              " making it home to some of the most famous museums, monuments, architectural feats"
              " and historical sites in the world.",textAlign: TextAlign.center,
            style:TextStyle(
              color:Colors.pinkAccent,
              fontFamily: 'Pacifico',
              fontSize: 18,
            ),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 20),
          child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children:[
                      Container(
                        //padding: const EdgeInsets.only(bottom:8),
                        child:Text('Wish to book a tour!! Click the icon',textAlign: TextAlign.center,
                          style: TextStyle(
                            color:Colors.white,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
    IconButton(
    icon:Icon(Icons.flight_takeoff,
    color: Colors.white,),
    onPressed: (){
    Navigator.pushNamed(context,'/second');
    },
    ),
              ]
          ),
        ),
              Padding(
                padding: EdgeInsets.all(0),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children:[
                          Container(
                            //padding: const EdgeInsets.only(bottom:1),
                            child:Text('Press the Heart to like this page!!',textAlign: TextAlign.center,
                              style: TextStyle(
                                color:Colors.white,
                              ),
              ),
    ),
      ],
                          ),
                      ),
      IconButton(
        icon:Icon(Icons.favorite,
          color: Colors.red,
        ),
        onPressed: incrementCounter,
      ),
      SizedBox(
        width:18,
        child: Container(
          child: Text('$counter',
            style: TextStyle(
              color: Colors.white,
            ),
          ),

        ),
      ),
      ]
      ),
              ),
      ],
                    ),
    );
  }
}

class Page2 extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(title:Text('Trip Details')),
      body: ListView(
        children: [
        Image.asset('assets/collage.jpg',
        fit: BoxFit.cover,
          height:400,
          width:200,
      ),
      Padding(
        padding: EdgeInsets.only(top:5),
        child:Text("Trip Details",textAlign: TextAlign.center,
          style:TextStyle(
            color:Colors.yellow,
            fontFamily: 'Pacifico',
            fontSize: 20,
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(top:10),
        child:Text('''
         Trip Start Date : Oct 1
         Trip End Date: Oct 10
         Cost: 2000 dollars/person''',textAlign: TextAlign.center,
          style:TextStyle(
            color:Colors.white,
          ),
        ),
      ),

      Padding(
        padding: EdgeInsets.all(32),
        child: Row(
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children:[
                    Container(
                      padding: const EdgeInsets.only(top:10),
                      child:Text('Homepage ',
                        style: TextStyle(
                          color:Colors.lightBlueAccent,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              IconButton(
                icon:Icon(Icons.home,
                  color: Colors.lightBlueAccent,),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),
            ]
        ),
      ),
    ],
      ),
  );
  }
}

